#!/usr/bin/env python3

import serial
from textwrap import wrap
import pympris
import dbus
from dbus.mainloop.glib import DBusGMainLoop

import pyautogui
pyautogui.FAILSAFE=False

import sys
import mouse
from bitstring import Bits
import argparse
import configparser
import os
import shutil
import subprocess

class iPointDriver():
	def __init__(self, port='/dev/ttyS0', keymap='~/.i-point.ini'):
		self.port=port
		self.keymapFile=os.path.expanduser(keymap)
		self.keymap=configparser.ConfigParser()
		if os.path.exists(self.keymapFile):
			self.keymap.read(self.keymapFile)
		else:
			shutil.copyfile("keymap.ini", self.keymapFile)
			self.keymap.read(self.keymapFile)
		## Automatically update keymap if changes happen
		defaultKeymap=configparser.ConfigParser()
		defaultKeymap.read("keymap.ini")
		for section in defaultKeymap.sections():
			for key in defaultKeymap.items(section):
				key=key[0]
				if not section in self.keymap.sections():
					self.keymap.add_section(section)
				if not key in self.keymap[section]:
					self.keymap[section][key]=defaultKeymap[section][key]
					self.keymap.write(open(self.keymapFile, "w+"))

	## Values of final byte for button presses
	buttons = {
		"c0": "rightClick",
		"80": "rightClick",
		"c1": "1",
		"c2": "2",
		"c3": "3",
		"c4": "4",
		"c5": "5",
		"c6": "6",
		"c7": "7",
		"c8": "8",
		"c9": "9",
		"ca": "esc",
		"cb": "0",
		"cc": "enter",
		"cd": "-",
		"ce": "*",
		"cf": "+"
	}
	## Get active media player
	def getMediaPlayer(self):
		dbus_loop = DBusGMainLoop()
		bus = dbus.SessionBus(mainloop=dbus_loop)
		player_ids = list(pympris.available_players())
		## Get currently playing player
		for player in player_ids:
			if pympris.MediaPlayer(player, bus).player.PlaybackStatus=="Playing" or pympris.MediaPlayer(player, bus).player.PlaybackStatus=="Paused":
				return pympris.MediaPlayer(player, bus)
		return None
	## Handle button presses
	def handleButton(self, button):
		mp=self.getMediaPlayer()
		print(button)
		## Media control
		try:
			## Volume up
			if button=="+" and mp.player.CanGoNext:
				mp.player.Next()
			## Volume down
			if button=="-" and mp.player.CanGoPrevious:
				mp.player.Previous()
			## Play/pause
			if button=="*" and mp.player.CanPause:
				mp.player.PlayPause()
		except Exception:
			pass
		## Right click
		if button=="rightClick":
			pyautogui.click(button='right')
		## Number buttons
		if button in ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "esc", "enter"]:
			action = self.keymap["buttons"][button]
			if action[0]=="~":
				print(f"Running: {action}")
				subprocess.Popen(action[1:].split())
			elif action[0]=="$":
				print(f"Running (Python): {action}")
				exec(action[1:])
			else:
				pyautogui.press(action)
	def listen(self):
		with serial.Serial(self.port, 1200, timeout=0.1) as ser:
			while True:
				## Read first byte
				dataRaw = ser.read(1)
				## Button press, read 3 more bytes
				if dataRaw.hex()=="d0":
					dataRaw += ser.read(3)
				## Mouse movement, read 2 more bytes
				elif len(dataRaw.hex()) > 0 and dataRaw.hex()[0] in ["c", "b", "e"]:
					dataRaw += ser.read(2)
				## Courrupt data, ignore
				else:
					continue
				data = dataRaw.hex()
				dataPairs=wrap(data, 2)
				dataBin=wrap(bin(int.from_bytes(dataRaw, byteorder=sys.byteorder))[2:],8)
				## Got nothing, try again
				if len(dataPairs)<3:
					continue
				## Small remote buttons
				if data=="d08080":
					pyautogui.mouseDown(button="right")
				elif data=="c08080":
					pyautogui.mouseUp(button="left")
					pyautogui.mouseUp(button="right")
				## Button was pressed, run button handler
				elif dataPairs[0]=="d0" and len(dataPairs)==4:
					self.handleButton(self.buttons[dataPairs[3]])
				## Mouse movement
				elif dataBin[0][:2] == "10" and dataBin[1][:2] == "10":
					## Get left click status
					if dataBin[2][2]=="1":
						pyautogui.mouseDown()
					else:
						pyautogui.mouseUp()
					xMove=0
					yMove=0
					## Mouse movement, run minimal mouse-only mode for smoother movement
					y=Bits(bin=dataBin[0][2:]+dataBin[2][4:6]).int
					x=Bits(bin=dataBin[1][2:]+dataBin[2][6:8]).int
					while abs(x) > 0 or abs(y) > 0:
						dataRaw=ser.read(3)
						dataBin=wrap(bin(int.from_bytes(dataRaw, byteorder=sys.byteorder))[2:],8)
						if len(dataBin)<3:
							break
						## Get X and Y values
						y=Bits(bin=dataBin[0][2:]+dataBin[2][4:6]).int
						x=Bits(bin=dataBin[1][2:]+dataBin[2][6:8]).int
						## Move mouse
						mouse.move(x, y, absolute=False, duration=0)
				## Events got combined
				else:
					print(f"Nonsense ({str(dataBin)})")

def main():
	parser=argparse.ArgumentParser(description="Driver for I-Point remote")
	parser.add_argument("--device", metavar="Device file", nargs='?', help="Location of the device file", default="/dev/ttyS0")
	parser.add_argument("--keymap", nargs="?", help="Keymap file to use (default: ~/.i-point.ini)", default="~/.i-point.ini")
	args=parser.parse_args()
	driver = iPointDriver(port=args.device, keymap=os.path.expanduser(args.keymap))
	driver.listen()

if __name__=="__main__":
	main()
