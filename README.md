# Driver for the I-Point serial IR presentation remote
![Photo of the remote and receiver](i-point.png)
## Currently supports
- Media control with - as previous track, + as next track, and * as play/pause
- Enter and esc buttons press their respective keys
- Number buttons can be configured to type characters or run commands
- Right and left click
- Mouse movement works with some strange behavior


## Configuration
The number buttons can be remapped by editing `.i-point.ini` in your home directory. The action of a button can be a key, a Python command prefaced by `$`, or a shell command prefaced by `~`.
